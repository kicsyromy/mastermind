use crate::utilities::natural_compare;

use std::error::Error;
use std::fs;
use std::path::PathBuf;

pub const VIDEO_FILE_EXTENSIONS: [&str; 3] = ["mkv", "avi", "mp4"];

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum FileType {
    File = 0x1,
    Folder = 0x2,
    _Other = 0x4,
}

pub fn get_content(
    path: &str,
    sorted: bool,
    filters: Option<&[&str]>,
) -> Result<(Vec<String>, Vec<String>), Box<dyn Error>> {
    let folders = get_content_folders(path, sorted)?;
    let files = get_content_files(path, sorted, filters)?;

    Ok((files, folders))
}

pub fn get_content_files(
    path: &str,
    sorted: bool,
    filters: Option<&[&str]>,
) -> Result<Vec<String>, Box<dyn Error>> {
    let path: PathBuf = [path].iter().collect();

    let mut files = vec![];

    for entry in fs::read_dir(path)? {
        let entry = entry?;

        let path = entry.path();
        let metadata = fs::metadata(&path)?;
        if metadata.is_file() && path.extension().is_some() {
            let extension = path.extension().unwrap().to_str().unwrap();
            let file_name =
                path.file_stem().unwrap().to_str().unwrap().to_string() + "." + extension;

            if filters.is_some() {
                if filters
                    .unwrap()
                    .contains(&extension.to_lowercase().as_str())
                {
                    files.push(file_name);
                }
            } else {
                files.push(file_name);
            }
        }
    }

    if sorted {
        files.sort_unstable_by(natural_compare);
    }

    Ok(files)
}

pub fn get_content_folders(path: &str, sorted: bool) -> Result<Vec<String>, Box<dyn Error>> {
    let path: PathBuf = [path].iter().collect();

    let mut folders = vec![];

    for entry in fs::read_dir(path)? {
        let entry = entry?;

        let path = entry.path();
        let metadata = fs::metadata(&path)?;
        if metadata.is_dir() {
            let folder_name = path.file_stem().unwrap().to_str().unwrap().to_string();
            folders.push(folder_name);
        }
    }

    if sorted {
        folders.sort_unstable_by(natural_compare);
    }

    Ok(folders)
}
