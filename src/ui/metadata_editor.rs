use crate::ui::list_item::{Column, ListItem};
use crate::ui::main_window::MainWindow;
use cursive::event::{Event, Key};
use cursive::traits::*;
use cursive::view::SizeConstraint;
use cursive::views::*;
use std::error::Error;
use std::ops::DerefMut;

pub struct MetadataEditor<'a> {
    season_edit_view: &'a mut EditView,
    content: &'a mut ListView,
    screen: &'a mut StackView,
    dialog: &'a mut Dialog,
}

impl<'a> MetadataEditor<'a> {
    pub fn new(screen: &mut StackView) -> Result<Self, Box<dyn Error>> {
        let dialog = OnEventView::new(
            Dialog::new()
                .title("Edit metadata")
                .content(
                    LinearLayout::vertical()
                        .child(PaddedView::lrtb(1, 0, 0, 0, TextView::new("Season")))
                        .child(PaddedView::lrtb(1, 0, 0, 1, EditView::new()))
                        .child(ScrollView::new(ListView::new())),
                )
                .button("OK", |s| {
                    let main_window = s.user_data::<MainWindow>().unwrap();

                    let files = unsafe {
                        std::mem::transmute::<usize, &mut ListView>(
                            main_window.metadata_editor.content as *mut _ as usize,
                        )
                    };
                    for episode in files.children().iter().enumerate() {
                        let index = episode.0 as i32;
                        let episode = episode.1;
                        match episode {
                            ListChild::Row(_, view) => {
                                let list_item = view.downcast_ref::<ListItem>().unwrap();
                                match &list_item.columns()[1] {
                                    Column::String(file_name) => {
                                        main_window
                                            .add_episode(
                                                main_window.metadata_editor.season_edit_view.get_content().parse::<i32>().unwrap(),
                                                index + 1,
                                                file_name.as_str(),
                                            )
                                            .unwrap();
                                    }
                                    _ => {}
                                }
                            }
                            _ => {}
                        }
                    }

                    main_window.left_pane.reset_highlight().unwrap();
                    main_window.metadata_editor.close();
                }),
        )
        .on_pre_event(Event::Key(Key::Esc), |s| {
            let this = &mut s.user_data::<MainWindow>().unwrap().metadata_editor;
            this.close();
        })
        .on_pre_event(Event::Key(Key::Tab), |s| {
            let _this = &mut s.user_data::<MainWindow>().unwrap().metadata_editor;
            // TODO: If the list is focused move the focus to the buttons
        });

        screen.add_layer(
            dialog
                .with_name("metadata_editor")
                .resized(SizeConstraint::AtLeast(70), SizeConstraint::AtLeast(20)),
        );

        let mut dialog = screen
            .get_mut(LayerPosition::FromFront(0))
            .ok_or("MetadataEditor: Failed to get dialog from main stack view")?
            .downcast_mut::<ResizedView<NamedView<OnEventView<Dialog>>>>()
            .ok_or("MetadataEditor: Failed to get dialog from main stack view, invalid cast")?
            .get_inner_mut()
            .get_mut();

        let layout = dialog
            .get_inner_mut()
            .get_content_mut()
            .downcast_mut::<LinearLayout>()
            .ok_or("MetadataEditor: Failed to get layout from dialog")?;

        let season_edit_view = layout
            .get_child_mut(1)
            .ok_or("MetadataEditor: Invalid index when extracting title_edit_view")?
            .downcast_mut::<PaddedView<EditView>>()
            .ok_or("MetadataEditor: Failed to get title_edit_view, invalid index or downcast")?
            .get_inner_mut() as *mut _ as usize;

        let list_view = layout
            .get_child_mut(2)
            .ok_or("MetadataEditor: Invalid index when extracting list_view")?
            .downcast_mut::<ScrollView<ListView>>()
            .ok_or("MetadataEditor: Failed to get list_view, invalid index or downcast")?
            .get_inner_mut() as *mut _ as usize;

        let this = MetadataEditor {
            season_edit_view: unsafe { std::mem::transmute(season_edit_view as *mut ()) },
            content: unsafe { std::mem::transmute(list_view as *mut ()) },
            screen: unsafe { std::mem::transmute(screen as *mut _ as usize as *mut ()) },
            dialog: unsafe {
                std::mem::transmute(dialog.get_inner_mut() as *mut _ as usize as *mut ())
            },
        };

        Ok(this)
    }

    pub fn open(&mut self) {
        let layer = self.screen.find_layer_from_name("metadata_editor").unwrap();
        self.screen.move_to_front(layer);
    }

    pub fn close(&mut self) {
        let layer = self.screen.find_layer_from_name("metadata_editor").unwrap();
        self.screen.move_to_back(layer);
    }

    pub fn add_list_item(&mut self, file_name: String) {
        self.content.add_child(
            "",
            ListItem::new(vec![
                Column::View(Box::new(
                    EditView::new().resized(SizeConstraint::Fixed(3), SizeConstraint::Fixed(1)),
                )),
                Column::String(file_name),
            ]),
        );
        let list_view = self.content as *mut _ as usize;
        let item = self.content.row_mut(self.content.len() - 1);
        match item {
            ListChild::Row(_, view) => {
                view.deref_mut()
                    .downcast_mut::<ListItem>()
                    .unwrap()
                    .set_parent(unsafe { std::mem::transmute(list_view as *mut ()) });
            }
            _ => {}
        }
    }

    pub fn clear(&mut self) {
        self.season_edit_view.set_content("");
        self.content.clear();
    }
}
