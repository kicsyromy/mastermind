use crate::ui::folder_browser::FolderBrowser;
use crate::ui::left_pane::LeftPane;
use crate::ui::metadata_editor::MetadataEditor;
use crate::ui::right_pane::RightPane;

use cursive::event::*;
use cursive::menu::*;
use cursive::traits::*;
use cursive::views::*;
use cursive::Cursive;
use std::error::Error;
use std::ops::{DerefMut};
use std::path::PathBuf;

pub struct MainWindow<'a> {
    pub layout: &'a mut LinearLayout,
    pub left_pane: LeftPane<'a>,
    pub right_pane: RightPane<'a>,
    pub screen: &'a mut StackView,
    pub folder_browser: FolderBrowser<'a>,
    pub metadata_editor: MetadataEditor<'a>,
}

impl<'a> MainWindow<'a> {
    pub fn new(cursive: &mut Cursive) -> Result<Self, Box<dyn Error>> {
        cursive.add_fullscreen_layer(StackView::new().with_name("screen"));

        cursive.set_autohide_menu(false);
        Self::populate_menubar(cursive.menubar())?;

        let mut main_layout = LinearLayout::horizontal();
        let left_pane = LeftPane::new(&mut main_layout)?;
        let right_pane = RightPane::new(&mut main_layout)?;

        let mut screen = cursive.find_name::<StackView>("screen").unwrap();
        let folder_browser = FolderBrowser::new(screen.deref_mut())?;
        let metadata_editor = MetadataEditor::new(screen.deref_mut())?;
        screen.deref_mut().add_fullscreen_layer(main_layout);

        let main_layout = screen
            .get_mut(LayerPosition::FromFront(0))
            .unwrap()
            .downcast_mut::<LinearLayout>()
            .unwrap() as *mut _ as usize;
        let screen = screen.deref_mut() as *mut _ as usize;

        cursive.add_global_callback(Event::Char('q'), |s| s.quit());
        cursive.add_global_callback(Event::Key(Key::F1), |s| s.select_menubar());
        cursive.add_global_callback(Event::Key(Key::F12), |s| s.show_debug_console());

        Ok(MainWindow {
            layout: unsafe { std::mem::transmute(main_layout as *mut ()) },
            left_pane,
            right_pane,
            screen: unsafe { std::mem::transmute(screen as *mut ()) },
            folder_browser,
            metadata_editor,
        })
    }

    pub fn load_content(&mut self, path: &str, tv_show_name: &str) -> Result<(), Box<dyn Error>> {
        self.left_pane.load_content(path)?;
        self.right_pane.load_metadata(tv_show_name)?;

        Ok(())
    }

    pub fn add_episode(
        &mut self,
        season: i32,
        episode_number: i32,
        source_file: &str,
    ) -> Result<(), Box<dyn Error>> {
        let root_path = self.left_pane.root_path();
        self.right_pane.add_episode(season, episode_number, [root_path, source_file].iter().collect::<PathBuf>())
    }

    fn populate_menubar(menubar: &mut Menubar) -> Result<(), Box<dyn Error>> {
        menubar.add_subtree(
            "File",
            MenuTree::new()
                .leaf("Open Content...", |s| {
                    let this = s.user_data::<MainWindow>().unwrap();
                    let folder_browser = &mut this.folder_browser;

                    folder_browser.set_title("Open Content...");
                    folder_browser.open();
                })
                .delimiter()
                .leaf("Quit", |s| s.quit()),
        );
        menubar.add_subtree(
            "Edit",
            MenuTree::new()
                .leaf("Update selected...", |s| {
                    let this = s.user_data::<MainWindow>().unwrap();
                    this.metadata_editor.clear();

                    let mut selected_entries = this.left_pane.selected_entries().unwrap();
                    for selected_entry in selected_entries.1.drain(..) {
                        this.metadata_editor.add_list_item(selected_entry);
                    }

                    this.metadata_editor.open();
                })
                .leaf("Apply updates...", |_| {}),
        );

        Ok(())
    }
}
