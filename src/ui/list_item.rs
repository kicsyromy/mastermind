use cursive::direction::Direction;
use cursive::event::{Event, EventResult};
use cursive::theme::ColorStyle;
use cursive::utils::markup::StyledString;
use cursive::views::{ListChild, ListView};
use cursive::*;

pub enum Column {
    String(String),
    View(Box<dyn View>),
}

impl Into<Column> for String {
    fn into(self) -> Column {
        Column::String(self)
    }
}

impl Into<Column> for &str {
    fn into(self) -> Column {
        Column::String(self.to_string())
    }
}

pub struct ListItem<'a> {
    columns: Vec<Column>,
    list_view: Option<&'a mut ListView>,
    highlighted: bool,
}

impl<'a> ListItem<'a> {
    pub fn new(columns: Vec<Column>) -> Self {
        Self {
            columns,
            list_view: None,
            highlighted: false,
        }
    }

    pub fn set_parent(&mut self, list_view: &'a mut ListView) {
        self.list_view = Some(list_view);
    }

    pub fn columns(&self) -> &[Column] {
        self.columns.as_slice()
    }

    pub fn has_focus(&self) -> bool {
        let has_focus;
        if self.list_view.is_some() {
            let list_view = self.list_view.as_ref().unwrap();
            let focused_child_index = list_view.focus();
            let focused_child = list_view.get_row(focused_child_index);
            match focused_child {
                ListChild::Row(_, list_item) => {
                    let list_item = list_item.downcast_ref::<ListItem>().unwrap();
                    has_focus = (list_item as *const _ as usize) == (self as *const _ as usize);
                }
                _ => {
                    has_focus = false;
                }
            }
        } else {
            has_focus = false;
        }

        has_focus
    }

    fn swap_highlight(&mut self) {
        self.highlighted = !self.highlighted;
    }

    pub fn set_highlight(&mut self, highlight: bool) {
        self.highlighted = highlight;
    }

    pub fn is_highlighted(&self) -> bool {
        self.highlighted
    }
}

impl View for ListItem<'static> {
    fn draw(&self, printer: &Printer) {
        if self.columns.is_empty() {
            return;
        }

        let mut printer = printer.clone();
        for column in self.columns.iter().enumerate() {
            let index = column.0;
            let column = column.1;

            match column {
                Column::String(text) => {
                    let mut column_text = text.clone();
                    if index < self.columns.len() - 1 {
                        column_text.push_str(" | ");
                    }

                    let column_text_len = column_text.chars().count();
                    let styled_string = if printer.focused {
                        StyledString::styled(column_text, ColorStyle::highlight())
                    } else {
                        if self.highlighted {
                            StyledString::styled(column_text, ColorStyle::title_secondary())
                        } else {
                            StyledString::styled(column_text, ColorStyle::primary())
                        }
                    };
                    printer.print_styled(Vec2::new(0, 0), (&styled_string).into());
                    printer.offset.x += column_text_len;
                }
                Column::View(view) => {
                    view.draw(&printer);
                    // FIXME: Just assume the child view column has a width of 3 (+ 1 for padding)
                    //        and height of 1... no idea how this can be done properly
                    printer.offset.x += 4;
                }
            }
        }
    }

    fn layout(&mut self, layout: Vec2) {
        for child in &mut self.columns {
            match child {
                Column::View(view) => {
                    view.layout(layout.clone());
                }
                _ => {}
            }
        }
    }

    fn needs_relayout(&self) -> bool {
        true
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        let mut line_length = 0 as usize;
        let mut max_height = 2 as usize;

        for column in &mut self.columns {
            match column {
                Column::String(string) => {
                    line_length += string.chars().count();
                }
                Column::View(view) => {
                    let child_size = view.required_size(constraint);
                    line_length += child_size.x + 1;
                    if child_size.y > max_height {
                        max_height = child_size.y;
                    }
                }
            }
        }

        Vec2 {
            x: line_length,
            y: max_height,
        }
    }

    fn on_event(&mut self, e: Event) -> EventResult {
        match e {
            Event::Char(_) => {}
            Event::Key(key) => {
                if self.list_view.is_some() {
                    match key {
                        event::Key::Ins => {
                            self.swap_highlight();

                            let list_view = self.list_view.as_mut().unwrap();
                            list_view.on_event(Event::Key(event::Key::Down));

                            return EventResult::Consumed(None);
                        }
                        _ => {}
                    }
                }
            }
            _ => {}
        }

        EventResult::Ignored
    }

    fn take_focus(&mut self, _source: Direction) -> bool {
        true
    }

    fn type_name(&self) -> &'static str {
        "ListItem"
    }
}
