use crate::ui::file_system;
use crate::ui::file_system::FileType;
use crate::ui::main_window::MainWindow;

use cursive::direction::{Absolute, Direction};
use cursive::event::*;
use cursive::traits::*;
use cursive::view::SizeConstraint;
use cursive::views::*;
use cursive::Cursive;
use std::error::Error;
use std::ops::{Deref, DerefMut};
use std::path::PathBuf;

struct Entry {
    pub name: String,
    pub kind: FileType,
}

pub struct FolderBrowser<'a> {
    pub path_bar: &'a mut EditView,
    pub tv_show_name: &'a mut EditView,

    folder_list: &'a mut SelectView<Entry>,
    screen: &'a mut StackView,
    folder_browser_view: &'a mut Dialog,
}

impl<'a> FolderBrowser<'a> {
    pub fn new(screen: &mut StackView) -> Result<Self, Box<dyn Error>> {
        let mut path_bar = EditView::new();
        path_bar.set_on_submit(|s, path| {
            let this = &mut s.user_data::<MainWindow>().unwrap().folder_browser;
            this.set_path(path).unwrap();
        });

        let mut folder_list = SelectView::<Entry>::new();
        folder_list.set_on_submit(|s: &mut Cursive, item: &Entry| {
            let this = &mut s.user_data::<MainWindow>().unwrap().folder_browser;

            let current_path = this.path_bar.get_content();
            let folder = &item.name;
            let kind = item.kind;

            if kind == FileType::Folder {
                let new_path;
                if folder == ".." {
                    let path: PathBuf = [current_path.deref()].iter().collect();
                    new_path = path.parent().unwrap().to_str().unwrap().to_string();
                } else {
                    new_path = format!("{}/{}", current_path.deref().trim_end_matches('/'), folder);
                }

                this.set_path(new_path).unwrap();
            }
        });

        let mut main_layout = LinearLayout::vertical();
        main_layout.add_child(PaddedView::lrtb(0, 0, 0, 1, path_bar));
        main_layout.add_child(TextView::new("TV Show"));
        main_layout.add_child(PaddedView::lrtb(0, 0, 0, 1, EditView::new()));
        main_layout.add_child(ScrollView::new(folder_list));

        let dialog_event_view = OnEventView::new(
            Dialog::new()
                .title("")
                .content(main_layout)
                .button("Cancel", |s| {
                    let main_window = s.user_data::<MainWindow>().unwrap();
                    main_window.folder_browser.set_visible(false);
                })
                .button("Open", |s| {
                    let main_window = s.user_data::<MainWindow>().unwrap();
                    main_window.folder_browser.set_visible(false);
                    main_window
                        .load_content(
                            main_window.folder_browser.get_current_path().as_str(),
                            main_window.folder_browser.get_current_tv_show().as_str(),
                        )
                        .unwrap();
                })
                .with_name("folder_browser"),
        )
        .on_pre_event(Event::Key(Key::Esc), |s| {
            let this = &mut s.user_data::<MainWindow>().unwrap().folder_browser;
            this.close();
        });

        screen.add_layer(
            dialog_event_view.resized(SizeConstraint::AtLeast(70), SizeConstraint::Free),
        );

        let main_view = screen
            .get_mut(LayerPosition::FromFront(0))
            .unwrap()
            .downcast_mut::<ResizedView<OnEventView<NamedView<Dialog>>>>()
            .unwrap()
            .get_inner_mut()
            .deref_mut()
            .get_inner_mut()
            .get_mut()
            .deref_mut() as *mut _ as usize;

        let view: &mut Dialog = unsafe { std::mem::transmute(main_view as *mut ()) };
        let path_bar = view
            .get_content_mut()
            .downcast_mut::<LinearLayout>()
            .unwrap()
            .get_child_mut(0)
            .unwrap()
            .downcast_mut::<PaddedView<EditView>>()
            .unwrap()
            .get_inner_mut() as *mut _ as usize;
        let tv_show_name = view
            .get_content_mut()
            .downcast_mut::<LinearLayout>()
            .unwrap()
            .get_child_mut(2)
            .unwrap()
            .downcast_mut::<PaddedView<EditView>>()
            .unwrap()
            .get_inner_mut() as *mut _ as usize;
        let folder_list = view
            .get_content_mut()
            .downcast_mut::<LinearLayout>()
            .unwrap()
            .get_child_mut(3)
            .unwrap()
            .downcast_mut::<ScrollView<SelectView<Entry>>>()
            .unwrap()
            .get_inner_mut() as *mut _ as usize;

        let mut this = FolderBrowser {
            path_bar: unsafe { std::mem::transmute(path_bar as *mut ()) },
            tv_show_name: unsafe { std::mem::transmute(tv_show_name as *mut ()) },
            folder_list: unsafe { std::mem::transmute(folder_list as *mut ()) },
            screen: unsafe { std::mem::transmute(screen as *mut _ as usize as *mut ()) },
            folder_browser_view: unsafe { std::mem::transmute(main_view as *mut ()) },
        };

        // this.set_path(env::current_dir().unwrap()).unwrap();
        this.set_path("/run/media/kicsyromy").unwrap();

        Ok(this)
    }

    pub fn open(&mut self) {
        let layer = self.screen.find_layer_from_name("folder_browser").unwrap();
        self.screen.move_to_front(layer);
    }

    pub fn close(&mut self) {
        let layer = self.screen.find_layer_from_name("folder_browser").unwrap();
        self.screen.move_to_back(layer);
    }

    pub fn is_open(&mut self) -> bool {
        let layer = self.screen.find_layer_from_name("folder_browser").unwrap();
        let required_position = LayerPosition::FromBack(self.screen.len() - 1);

        layer == required_position
    }

    fn set_visible(&mut self, visible: bool) {
        let layer = self.screen.find_layer_from_name("folder_browser").unwrap();
        if visible {
            self.screen.move_to_front(layer);
            self.path_bar.take_focus(Direction::Abs(Absolute::None));
        } else {
            self.screen.move_to_back(layer);
        }
    }

    fn populate_folder_list<P: Into<PathBuf>>(&mut self, path: P) -> Result<(), Box<dyn Error>> {
        let path = path.into();

        self.folder_list.clear();

        if path.parent().is_some() {
            self.folder_list.add_item(
                "..",
                Entry {
                    name: "..".to_string(),
                    kind: FileType::Folder,
                },
            );
        }

        let (files, folders) = self.get_current_path_content()?;

        for item in folders {
            self.folder_list.add_item(
                item.clone(),
                Entry {
                    name: item,
                    kind: FileType::Folder,
                },
            );
        }

        for item in files {
            self.folder_list.add_item(
                item.clone(),
                Entry {
                    name: item,
                    kind: FileType::File,
                },
            );
        }

        Ok(())
    }

    pub fn get_current_path(&self) -> String {
        self.path_bar.get_content().deref().clone()
    }

    pub fn get_current_tv_show(&self) -> String {
        self.tv_show_name.get_content().deref().clone()
    }

    pub fn get_current_path_content(&self) -> Result<(Vec<String>, Vec<String>), Box<dyn Error>> {
        let current_path = self.get_current_path();
        file_system::get_content(
            current_path.as_str(),
            true,
            Some(&file_system::VIDEO_FILE_EXTENSIONS),
        )
    }

    pub fn set_path<P: Into<PathBuf>>(&mut self, path: P) -> Result<(), Box<dyn Error>> {
        let path = path.into();

        self.path_bar.set_content(
            path.to_str()
                .ok_or("FolderBrowser: Path contains invalid characters")?,
        );

        let last_path_separator = path.to_str().unwrap().rfind("/");
        if last_path_separator.is_some()
            && last_path_separator.unwrap() < path.to_str().unwrap().len() - 1
        {
            let potential_show_name = &path.to_str().unwrap()[last_path_separator.unwrap() + 1..];
            self.tv_show_name.set_content(potential_show_name);
        }

        self.populate_folder_list(path)?;

        Ok(())
    }

    pub fn set_title(&mut self, title: &str) {
        self.folder_browser_view.set_title(title);
    }
}
