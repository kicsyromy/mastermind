use crate::tv_show_content::TvShowContent;

use crate::ui::list_item::ListItem;
use cursive::align::HAlign;
use cursive::traits::*;
use cursive::views::*;
use std::error::Error;
use std::ops::DerefMut;
use std::path::PathBuf;

pub struct RightPane<'a> {
    content: &'a mut ListView,
    panel: &'a mut Panel<ScrollView<ListView>>,
    tv_show_name: String,
    model: TvShowContent,
}

impl<'a> RightPane<'a> {
    pub fn new(view: &mut LinearLayout) -> Result<Self, Box<dyn Error>> {
        let show_view = ListView::new();
        let show_pane = Panel::new(ScrollView::new(show_view))
            .title("")
            .title_position(HAlign::Right);

        view.add_child(show_pane.full_screen());

        let content = view
            .get_child_mut(view.len() - 1)
            .ok_or("RightPane: Failed to find show_view")?
            .downcast_mut::<ResizedView<Panel<ScrollView<ListView>>>>()
            .ok_or("RightPane: show_view does not have the correct data type")?
            .get_inner_mut()
            .get_inner_mut()
            .get_inner_mut() as *mut _ as usize;

        let panel = view
            .get_child_mut(view.len() - 1)
            .ok_or("RightPane: Failed to find show_view")?
            .downcast_mut::<ResizedView<Panel<ScrollView<ListView>>>>()
            .ok_or("RightPane: show_view does not have the correct data type")?
            .get_inner_mut() as *mut _ as usize;

        Ok(RightPane {
            content: unsafe { std::mem::transmute(content as *mut ()) },
            panel: unsafe { std::mem::transmute(panel as *mut ()) },
            tv_show_name: String::new(),
            model: TvShowContent::new(),
        })
    }

    // pub fn add_episode(&mut self, columns: Vec<Column>) {
    //     self.content.add_child("", ListItem::new(columns));
    //     let list_view = self.content as *mut _ as usize;
    //     let item = self.content.row_mut(self.content.len() - 1);
    //     match item {
    //         ListChild::Row(_, view) => {
    //             view.deref_mut()
    //                 .downcast_mut::<ListItem>()
    //                 .unwrap()
    //                 .set_parent(unsafe { std::mem::transmute(list_view as *mut ()) });
    //         }
    //         _ => {}
    //     }
    // }

    pub fn add_episode<P: Into<PathBuf>>(
        &mut self,
        season: i32,
        episode_number: i32,
        source_file: P,
    ) -> Result<(), Box<dyn Error>> {
        self.model
            .set_source_file(season, episode_number, source_file)?;
        self.content.add_child(
            "",
            ListItem::new(vec![
                "Season".into(),
                season.to_string().into(),
                episode_number.to_string().into(),
                self.model.episode_title(season, episode_number)?.into(),
            ]),
        );
        let list_view = self.content as *mut _ as usize;
        let item = self.content.row_mut(self.content.len() - 1);
        match item {
            ListChild::Row(_, view) => {
                view.deref_mut()
                    .downcast_mut::<ListItem>()
                    .unwrap()
                    .set_parent(unsafe { std::mem::transmute(list_view as *mut ()) });
            }
            _ => {}
        }

        Ok(())
    }

    pub fn load_metadata(&mut self, title: &str) -> Result<(), Box<dyn Error>> {
        self.panel.set_title(title);
        self.model.reset(title)?;

        Ok(())
    }
}
