use crate::ui::file_system;
use crate::ui::list_item::{Column, ListItem};
use crate::unfiltered_content::UnfilteredContent;

use cursive::align::HAlign;
use cursive::traits::*;
use cursive::views::*;
use std::error::Error;
use std::ops::DerefMut;
use std::path::PathBuf;

pub struct LeftPane<'a> {
    content: &'a mut ListView,
    path: String,
    panel: &'a mut Panel<ScrollView<ListView>>,
    model: UnfilteredContent,
}

impl<'a> LeftPane<'a> {
    pub fn new(view: &mut LinearLayout) -> Result<Self, Box<dyn Error>> {
        let show_view = ListView::new();
        let show_pane = Panel::new(ScrollView::new(show_view))
            .title("")
            .title_position(HAlign::Left);

        view.add_child(show_pane.full_screen());

        let panel = view
            .get_child_mut(view.len() - 1)
            .ok_or("LeftPane: Failed to find show_view")?
            .downcast_mut::<ResizedView<Panel<ScrollView<ListView>>>>()
            .ok_or("LeftPane: show_view does not have the correct data type")?
            .get_inner_mut();

        let show_view = panel.get_inner_mut().get_inner_mut() as *mut _ as usize;

        Ok(LeftPane {
            content: unsafe { std::mem::transmute(show_view as *mut ()) },
            path: String::new(),
            panel: unsafe { std::mem::transmute(panel as *mut _ as usize as *mut ()) },
            model: UnfilteredContent::new()
        })
    }

    fn add_list_item(&mut self, column: Vec<Column>) {
        self.content.add_child("", ListItem::new(column));
        let list_view = self.content as *mut _ as usize;
        let item = self.content.row_mut(self.content.len() - 1);
        match item {
            ListChild::Row(_, view) => {
                view.deref_mut()
                    .downcast_mut::<ListItem>()
                    .unwrap()
                    .set_parent(unsafe { std::mem::transmute(list_view as *mut ()) });
            }
            _ => {}
        }
    }

    fn update_model(&mut self, path: &str) -> Result<(), Box<dyn Error>> {
        let (files, folders) = file_system::get_content(path, true, Some(&file_system::VIDEO_FILE_EXTENSIONS))?;

        for folder in folders {
            let path = path.to_string() + "/" + folder.as_str();
            self.update_model(path.as_str())?;
        }

        let mut path = path.trim_start_matches(self.model.root_path());
        path = path.trim_start_matches('/');

        for file in files {
            self.model.push(if path.is_empty() { file } else { path.to_string() + "/" + file.as_str() });
        }

        Ok(())
    }

    pub fn load_content(&mut self, path: &str) -> Result<(), Box<dyn Error>> {
        self.path = path.to_string();
        self.content.clear();
        self.panel.set_title(path);
        self.model.clear();
        self.model.set_root_path(path);

        self.update_model(path)?;

        let file_list = self.model.file_list().to_vec();
        for file in file_list {
            self.add_list_item(vec![Column::String(file.clone())]);
        }

        Ok(())
    }

    pub fn root_path(&self) -> &str {
        self.path.as_str()
    }

    pub fn reset_highlight(&mut self) -> Result<(), Box<dyn Error>> {
        for i in 0..self.content.len() {
            let child = self.content.row_mut(i);
            match child {
                ListChild::Row(_, list_item) => {
                    let list_item = list_item.downcast_mut::<ListItem>().unwrap();
                    list_item.set_highlight(false);
                }
                _ => {}
            }
        }

        Ok(())
    }

    pub fn selected_entries(&self) -> Result<(PathBuf, Vec<String>), Box<dyn Error>> {
        let mut selected_indices = Vec::new();
        for child in self.content.children() {
            match child {
                ListChild::Row(_, list_item) => {
                    let list_item = list_item.downcast_ref::<ListItem>().unwrap();
                    if list_item.is_highlighted() {
                        let columns = &list_item.columns();
                        if columns.len() != 1 {
                            return Err("LeftPane: Invalid entry in content list".into());
                        }
                        let column = &list_item.columns()[0];
                        match column {
                            Column::String(str) => {
                                selected_indices.push(str.clone());
                            }
                            _ => {}
                        }
                    }
                }
                _ => {}
            }
        }

        Ok((self.model.root_path().into(), selected_indices))
    }
}
