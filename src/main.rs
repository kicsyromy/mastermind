mod tv_show;
mod tv_show_content;
mod tvdb;
mod ui;
mod unfiltered_content;
mod utilities;

use cursive::theme::{Color, PaletteColor, Theme};
use std::error::Error;
use std::thread::sleep;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // sleep(std::time::Duration::from_millis(5000 as u64));

    let mut theme = Theme::default();
    theme.palette[PaletteColor::Background] = Color::Rgb(0, 0, 0);
    //theme.palette[PaletteColor::View] = Color::Rgb(0, 0, 0);
    // theme.palette[PaletteColor::Highlight] = Color::Rgb(255, 255, 255);
    // theme.palette[PaletteColor::HighlightInactive] = Color::Rgb(255, 255, 255);
    // theme.palette[PaletteColor::HighlightText] = Color::Rgb(255, 255, 255);
    // theme.palette[PaletteColor::Primary] = Color::Rgb(255, 255, 255);
    // theme.palette[PaletteColor::Secondary] = Color::Rgb(255, 255, 255);
    // theme.palette[PaletteColor::Shadow] = Color::Rgb(255, 255, 255);
    // theme.palette[PaletteColor::Tertiary] = Color::Rgb(255, 255, 255);
    // theme.palette[PaletteColor::TitlePrimary] = Color::Rgb(255, 255, 255);
    // theme.palette[PaletteColor::TitleSecondary] = Color::Rgb(255, 255, 255);

    // Creates the cursive root - required for every application.
    // let mut siv = cursive::dummy();
    let mut siv = cursive::default();
    siv.set_theme(theme);

    let main_window = ui::main_window::MainWindow::new(&mut siv)?;
    siv.set_user_data(main_window);

    // Starts the event loop.
    siv.run();

    // let mut seasons = tvdb::get_seasons("Family Guy")?;
    // println!("{:?}", seasons);
    // let episodes = tvdb::get_episodes(&mut seasons[19])?;
    // println!("{:?}", episodes);

    Ok(())
}
