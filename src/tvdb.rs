use crate::tv_show::{Episode, Season};

use scraper::{Html, Selector};
use std::error::Error;

const _EPISODE_COLUMN_IDENTIFIER: usize = 1;
const EPISODE_COLUMN_TITLE: usize = 2;
const EPISODE_COLUMN_RELEASE_DATE: usize = 3;
const EPISODE_COLUMN_RUNTIME: usize = 4;

pub fn get_seasons(tv_show_title: &str) -> Result<Vec<Season>, Box<dyn Error>> {
    let mut result: Vec<Season> = vec![];

    let normalized_title = tv_show_title.to_lowercase().trim().replace(" ", "-");
    let response = async_std::task::block_on(async {
        reqwest::get(
            ("https://www.thetvdb.com/series/".to_string() + &*normalized_title.to_string())
                .as_str(),
        )
        .await?
        .text()
        .await
    })?;

    // Use CSS selectors to retrieve the HTML table that lists all seasons
    let html = Html::parse_document(response.as_str());
    let selector = Selector::parse("body>div>div>div>div>div>ul>li").unwrap();
    for element in html.select(&selector) {
        let content = &element.inner_html();
        // Two entries in the table are "non-seasons" so skip them
        if content.contains("unassigned") || content.contains("allseasons") {
            continue;
        }

        // Parse the table entry as an HTML fragment
        let document = Html::parse_fragment(content.as_str());

        // Use CSS selectors to extract bits of information from each entry in the table

        // Extract the link to the episode list by grabbing the heading (which actually contains the
        // link among some extra bits of info) and split it into sections until just the string with
        // the actual link remains. This feels like the most fragile piece of parsing.
        let season_link_selector = Selector::parse("h4").unwrap();
        let season_link = document
            .select(&season_link_selector)
            .next()
            .unwrap()
            .inner_html()
            .trim()
            .split("\n")
            .next()
            .unwrap()
            .split("\"")
            .skip(1)
            .next()
            .unwrap()
            .to_string();

        // Only extract seasons in official order
        if !(season_link.to_lowercase().contains("official/")) {
            continue;
        }

        // The number of episodes for each season shows up in a badge counter. The HTML element uses
        // a "span" class type
        let badge_selector = Selector::parse("span").unwrap();
        let episode_count = document
            .select(&badge_selector)
            .next()
            .ok_or("TVDB: Failed to parse episode count")?;
        let episode_count = episode_count.inner_html().trim().to_string();
        let episode_count = episode_count.parse::<i32>()?;

        // The next bit of HTML has both the hyperlink to the episode list for the season as well as
        // the season "name" which consists of the word "Season" and a number, with one exception
        // being "Season 0" which is called specials. The next bit of code extracts the latter with
        // special handling for "Season 0"
        let season_name_selector = Selector::parse("h4>a").unwrap();
        let season_name = document
            .select(&season_name_selector)
            .next()
            .ok_or("TVDB: Failed to parse season number")?
            .inner_html()
            .trim()
            .to_string();
        let season_number = if season_name.to_lowercase() == "specials" {
            "0"
        } else {
            season_name
                .split(" ")
                .skip(1)
                .next()
                .ok_or("TVDB: Invalid season name hit when parsing season number")?
        };
        let season_number = season_number.parse::<i32>()?;

        result.push(Season::new(
            season_number,
            episode_count,
            tv_show_title,
            season_link.as_str(),
        ));
    }

    Ok(result)
}

pub fn get_episodes(season: &mut Season) -> Result<(), Box<dyn Error>> {
    let mut result: Vec<Episode> = vec![];

    let response =
        async_std::task::block_on(async { reqwest::get(season.url.as_str()).await?.text().await })?;

    // Use CSS selectors to retrieve the HTML table that lists all episodes
    let html = Html::parse_document(response.as_str());
    let selector = Selector::parse("body>div>div>div>table>tbody>tr").unwrap();

    // Iterate over each entry in the table and split the columns by <td> tag
    for element in html.select(&selector) {
        let mut title = String::new();
        let mut release_date = String::new();
        let mut runtime: i32 = 0;
        for entry in element.inner_html().trim().split("<td>").enumerate() {
            let it = entry.0;
            let entry = entry.1.trim().trim_end_matches("</td>");
            match it {
                EPISODE_COLUMN_TITLE => {
                    // The title entry contains the link to episode details as well as the name.
                    // So split by new line and take the second entry
                    title = entry.split("\n").skip(1).next().unwrap().trim().to_string();
                }
                EPISODE_COLUMN_RELEASE_DATE => {
                    // The release date entry contains some extra unused bits, same as above split
                    // by the new line character, take the first entry and clean up the result
                    release_date = entry
                        .split("\n")
                        .next()
                        .unwrap()
                        .trim_start_matches("<div>")
                        .trim_end_matches("</div>")
                        .to_string();
                }
                EPISODE_COLUMN_RUNTIME => {
                    // This one is easy as it only contains the number of minutes, so take it as is
                    // and convert it to an integer
                    runtime = entry.trim().parse::<i32>()?;
                }
                _ => {
                    continue;
                }
            }
        }

        result.push(Episode::new(
            title.as_str(),
            season.tv_show_title.as_str(),
            result.len() as i32 + 1,
            season.number,
            release_date.as_str(),
            "%B %-d, %Y",
            runtime,
        )?);
    }

    season.episodes = Some(result);

    Ok(())
}
