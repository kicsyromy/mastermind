use std::path::PathBuf;

pub struct UnfilteredContent {
    path: PathBuf,
    file_list: Vec<String>,
}

impl UnfilteredContent {
    pub fn new() -> Self {
        Self {
            path: PathBuf::new(),
            file_list: Vec::new(),
        }
    }

    pub fn set_root_path<P: Into<PathBuf>>(&mut self, path: P) {
        self.path = path.into();
    }

    pub fn root_path(&self) -> &str {
        self.path.to_str().unwrap()
    }

    pub fn push<S: Into<String>>(&mut self, file: S) {
        self.file_list.push(file.into());
    }

    pub fn clear(&mut self) {
        self.path = PathBuf::new();
        self.file_list.clear();
    }

    pub fn file_list(&self) -> &[String] {
        &self.file_list[..]
    }
}
