use chrono::{Date, Duration, NaiveDate, Utc};
use std::error::Error;

#[derive(Debug)]
pub struct Season {
    pub number: i32,
    pub episode_count: i32,
    pub tv_show_title: String,
    pub url: String,
    pub episodes: Option<Vec<Episode>>,
}

impl Season {
    pub fn new(number: i32, episode_count: i32, tv_show_title: &str, url: &str) -> Self {
        Self {
            number,
            episode_count,
            tv_show_title: tv_show_title.to_string(),
            url: url.to_string(),
            episodes: None,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Episode {
    pub title: String,
    pub tv_show_title: String,
    pub number: i32,
    pub season: i32,
    pub aired: Option<Date<Utc>>,
    pub runtime: Duration,
}

impl Episode {
    pub fn new(
        title: &str,
        tv_show_title: &str,
        number: i32,
        season: i32,
        aired: &str,
        aired_format: &str,
        runtime_in_minutes: i32,
    ) -> Result<Self, Box<dyn Error>> {
        let air_date;
        let air_date_parse_result= NaiveDate::parse_from_str(aired, aired_format);
        if air_date_parse_result.is_ok() {
            air_date = Some(Date::from_utc(air_date_parse_result.unwrap(), Utc));
        } else {
            air_date = None;
        }
        Ok(Episode {
            title: title.to_string(),
            tv_show_title: tv_show_title.to_string(),
            number,
            season,
            aired: air_date,
            runtime: Duration::minutes(runtime_in_minutes as i64),
        })
    }
}
