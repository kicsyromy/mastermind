use crate::{tv_show, tvdb};
use std::path::PathBuf;

use std::collections::HashMap;
use std::error::Error;

#[derive(Clone)]
pub struct Episode {
    index: usize,
    source_file: PathBuf,
}

pub struct TvShowContent {
    name: String,
    episodes: HashMap<i32, Vec<Episode>>,
    metadata: Vec<tv_show::Season>,
}

impl TvShowContent {
    pub fn new() -> Self {
        Self {
            name: String::new(),
            episodes: HashMap::new(),
            metadata: Vec::new(),
        }
    }

    pub fn from_tv_show(tv_show: &str) -> Result<Self, Box<dyn Error>> {
        let mut this = Self::new();
        this.reset(tv_show)?;

        Ok(this)
    }

    pub fn reset(&mut self, tv_show: &str) -> Result<(), Box<dyn Error>> {
        self.name = tv_show.to_string();
        self.episodes.clear();
        self.metadata = tvdb::get_seasons(tv_show)?;

        for season in &mut self.metadata {
            let mut vec = Vec::new();
            vec.resize(
                season.episode_count as usize,
                Episode {
                    index: 0,
                    source_file: "".into(),
                },
            );
            for mut ep in vec.iter_mut().enumerate() {
                ep.1.index = ep.0;
            }
            let insert_result = self.episodes.insert(season.number, vec);
            if insert_result.is_some() {
                return Err("TvShowContent: Adding the same season twice should not happen, forgot to clear?".into());
            }
        }

        Ok(())
    }

    pub fn name(&self) -> &str {
        self.name.as_str()
    }

    pub fn episode_title(&self, season: i32, episode_number: i32) -> Result<&str, Box<dyn Error>> {
        let episode = self.get_episode(season, episode_number)?;
        let season_info = self.metadata[season as usize].episodes.as_ref().ok_or(format!(
            "TvShowContent: Metadata not loaded for season {} of {}",
            season, &self.name
        ))?;
        Ok(season_info[episode.index].title.as_str())
    }

    // pub fn all_episodes(&self) -> Vec<Option<Episode>> {
    //     let mut result = Vec::new();
    //     for episode in self.episodes.iter() {
    //         result.append(&mut episode.1.clone());
    //     }
    //
    //     result
    // }
    //
    // pub fn episodes(&self, season: i32) -> Vec<Option<Episode>> {
    //     let mut result = Vec::new();
    //     let season = self.episodes.get(&season);
    //     if season.is_some() {
    //         result = season.unwrap().clone();
    //     }
    //
    //     result
    // }

    pub fn set_source_file<P: Into<PathBuf>>(
        &mut self,
        season: i32,
        episode_number: i32,
        source_file: P,
    ) -> Result<(), Box<dyn Error>> {
        let episode = self.update_and_get_episode(season, episode_number)?;
        episode.source_file = source_file.into();

        Ok(())
    }

    fn get_episode(&self, season: i32, episode_number: i32) -> Result<&Episode, Box<dyn Error>> {
        if season < 0 || season >= self.metadata.len() as i32 {
            return Err(format!(
                "TvShowContent: Season {} is out of range for {}",
                season, &self.name
            )
            .into());
        }

        let season_data = &self.metadata[season as usize];
        if episode_number < 1 || episode_number > season_data.episode_count {
            return Err(format!(
                "TvShowContent: Episode {} is out of range for season {} of {}",
                episode_number, season, &self.name
            )
            .into());
        }

        let episodes = self.episodes.get(&season).unwrap();
        let episode = &episodes[(episode_number - 1) as usize];

        Ok(episode)
    }

    fn update_and_get_episode(
        &mut self,
        season: i32,
        episode_number: i32,
    ) -> Result<&mut Episode, Box<dyn Error>> {
        if season < 0 || season >= self.metadata.len() as i32 {
            return Err(format!(
                "TvShowContent: Season {} is out of range for {}",
                season, &self.name
            )
            .into());
        }

        let season_data = &mut self.metadata[season as usize];
        if episode_number < 1 || episode_number > season_data.episode_count {
            return Err(format!(
                "TvShowContent: Episode {} is out of range for season {} of {}",
                episode_number, season, &self.name
            )
            .into());
        }

        if season_data.episodes.is_none() {
            tvdb::get_episodes(season_data)?;
        }

        let episodes = self.episodes.get_mut(&season).unwrap();
        let episode = &mut episodes[(episode_number - 1) as usize];

        Ok(episode)
    }
}
