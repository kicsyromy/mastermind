use std::cmp::Ordering;

pub fn natural_compare(left: &String, right: &String) -> Ordering {
    let left_elements = left.split_whitespace();
    let mut right_elements = right.split_whitespace();

    let mut order: Ordering = Ordering::Equal;
    for left_element in left_elements {
        let right_element = right_elements.next();

        if right_element.is_some() {
            let right_element = right_element.unwrap();
            if left_element != right_element {
                let left_number = left_element.parse::<i64>();
                let right_number = right_element.parse::<i64>();
                if left_number.is_ok() && right_number.is_ok() {
                    let left_number = left_number.unwrap();
                    let right_number = right_number.unwrap();
                    if left_number != right_number {
                        order = left_number.cmp(&right_number);
                        break;
                    }
                } else {
                    order = left_element.cmp(right_element);
                    if order != Ordering::Equal {
                        break;
                    }
                }
            }
        } else {
            order = Ordering::Greater;
            break;
        }
    }

    if order == Ordering::Equal && right_elements.next().is_some() {
        order = Ordering::Less;
    }

    order
}
